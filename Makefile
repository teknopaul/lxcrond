#
# Makefile lxcrond - a crond server for LXC containers
#

SRC=$(wildcard src/*.rs)

target/x86_64-unknown-linux-musl/release/lxcrond: $(SRC)
	cargo build --release --target=x86_64-unknown-linux-musl
	strip target/x86_64-unknown-linux-musl/release/lxcrond

.PHONY: clean install uninstall run test deb

run:
	cargo run

test:
	cargo test

deb:
	sudo deploy/build-deb.sh

clean:
	cargo clean
	rm -f target/
	mkdir target

install:
	sudo dpkg --install target/lxcrond-*.deb

uninstall:
	sudo dpkg --remove target/lxcrond-*.deb

