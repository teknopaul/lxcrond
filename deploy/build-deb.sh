#!/bin/bash -e
#
# Build a binary .deb package
##
test $(id -u) == "0" || (echo "Run as root" && exit 1) # requires bash -e

#
# The package name
#
name=lxcrond
arch=$(uname -m)

cd $(dirname $0)/..
project_root=$PWD

#
# Create a temporary build directory
#
tmp_dir=/tmp/$name-debbuild
rm -rf $tmp_dir
mkdir -p $tmp_dir/DEBIAN
test -d $tmp_dir

. ./version
sed -e "s/@PACKAGE_VERSION@/$VERSION/" $project_root/deploy/DEBIAN/control.in > $tmp_dir/DEBIAN/control
mkdir -p  $tmp_dir/etc/  $tmp_dir/sbin/
cp target/x86_64-unknown-linux-musl/release/lxcrond $tmp_dir/sbin/
cp etc/lxcrontab.dist $tmp_dir/etc/lxcrontab

size=$(du -sk $tmp_dir | cut -f 1)
sed -i -e "s/@SIZE@/$size/" $tmp_dir/DEBIAN/control

#
# setup conffiles
#
(
  cd $tmp_dir/
  find etc -type f | sed 's.^./.' > DEBIAN/conffiles
)

#
# Setup the installation package ownership here if it needs root
#
chown -R root.root $tmp_dir/

#
# Build the .deb
#
mkdir -p $project_root/target/
dpkg-deb --build $tmp_dir $project_root/target/$name-$VERSION-1.$arch.deb

test -f $project_root/target/$name-$VERSION-1.$arch.deb

echo "built $project_root/target/$name-$VERSION-1.$arch.deb"

if [ -n "$SUDO_USER" ]
then
  chown $SUDO_USER $project_root/target/$name-$VERSION-1.$arch.deb
fi

test -d $tmp_dir && rm -rf $tmp_dir
