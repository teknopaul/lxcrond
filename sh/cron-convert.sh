#!/bin/bash
#
# convert /etc/cron.*/* to a /etc/lxcrontab
#

function cron_script() {
  dir=$(dirname $1)
  alias=$(echo ${v/[^\.]*\./})
  echo "@${alias}    root    $1"
}

for cron_file in $(ls -1 /etc/cron.d/*)
do
  cat $cron_file
done

for script in $(ls -1 /etc/cron.*ly/*)
do
  cron_script $script
done

