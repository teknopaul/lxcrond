use crate::config::Job;
use crate::ticker;
use crate::exec::execute_job;

pub struct WatcherCron {
    jobs: Vec<Job>,
}

/// File watcher based on cron, i.e. ticks every 60 seconds, used when inotify fails
///
impl WatcherCron {

    pub fn new(jobs: Vec<Job>) -> WatcherCron {
        WatcherCron {
            jobs
        }
    }

    fn len(&self) -> usize {
        self.jobs.len()
    }

    pub fn run(&mut self) {
        if 0 == self.len() {
            return;
        }
        ticker::minute_sync();
        loop {
            self.current_jobs().iter().for_each(|j| {
                execute_job(j);
            });
            ticker::sleep_sync();
        }
    }

    pub fn current_jobs(&mut self) -> Vec<&Job> {
        let mut current_jobs = vec![];
        for job in &mut self.jobs {
            if job.file_spec().is_dir() && job.dir_has_files() {
                current_jobs.push(&*job);
            } else if job.file_has_changed() {
                current_jobs.push(&*job);
            }

        }
        current_jobs
    }

}
