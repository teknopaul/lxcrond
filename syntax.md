
## File format
 
A single file `/etc/lxcrontab` is read by lxcrond to determine the jobs to run.  

File format is similar to `/etc/crontab`, i.e. requires the user field.
Rows should contain  **time_spec** or a **file_spec**  a **username** and a **command** to run plus its args.
There should be no leading whitespace to the row.

N.B. lxcrond does not use or require a shell, so the command and its args is _not_ a bash command line.  
Neither pipes `|` nor subshells `$(...)` nor environment variables `$HOME` nor shell expansion e.g, `*` can be used in the command field. 

The command should consist of a command binary and its arguments. Escaping with `\\ ` and delimiting strings with  `""` _is_ provided 
by [cmdline_words_parser](https://crates.io/crates/cmdline_words_parser)

### time_spec 

**time_spec** is a familiar cron format consisting of 5 sub fields, `minute`, `hour`, `day_of_month`, `month_of_year` and `day_of_week`

	# m   h  dom mon dow  user       command
	23  1  *   *   *      teknopaul  /usr/bin/linci admin backup

Each field may be `*` to indicate _any_ value, as with standard cron.

`/` syntax is not currently supported, but a comma separated list of values is.

`lxcrond` also support standard aliases `@daily` etc per busybox, and the following bells and whistles.

- `@always`, `@minutely`, `@1min` - every minute
- `@5mins`, `@10mins`, `@15mins` - every 5, 10 or 15 minutes
- `@hourly` - every hour, on the hour
- `@daily`, `@midnight` - every day at midnight
- `@mondays`, `@tuesdays`, `@wednesdays`, `@thursdays`, `@fridays`, `@saturdays`, `@sundays` - given day at midnight
- `@semimonthly`, `@fortnightly` - N.B. these are the same run on the 1st and the 15th at midnight, not exactly every two weeks
- `@monthly` - 1st of the month at midnight
- `@semiannually`, `@biannually`  - 1st of January and July at midnight
- `@quarterly`  - 1st of January, April, July and October at midnight
- `@yearly`, `@annually`  - 1st of January at midnight
- `@never` - similar to commenting out a job, but the job is parsed and loaded

### file_spec

lxcrond also supports an absolute file name as the trigger, if the specified file is changed or created the job is executed.

	# path             user       command
	/etc/nginx.conf    root       /bin/restart-nginx

Support empty directories, if a file arrives in the directory run a job.
The job should delete or move the file or the job may get re-run every 60 seconds.

	# path             user       command
	/mnt/uploads       clam       /bin/scan-uploads
